import string
import gensim
import pyLDAvis.gensim
import argparse
import matplotlib.pyplot as plt
import numpy as np
from text_preprocessing import prepare_text

from database_reader import from_table
from doc2vec import load_doc2vec_model
from pprint import pprint
from nltk.stem.snowball import SpanishStemmer
from gensim.models import CoherenceModel
from timeit import default_timer as timer

# pandas.set_option('display.mpl_style', 'default')
NEWS_TABLE = 'news'
MALLET_PATH = '/home/mmedina/Programming/Python/Clustering/mallet-2.0.8/bin/mallet'
RANDOM_SEED = 2344


def lda_topic_modelling(num_topics, word_map, corpus):
    # The model
    # return gensim.models.LdaModel(corpus=corpus, num_topics=num_topics, id2word=word_map, passes=10,
    #                               update_every=0, iterations=1000, random_state=RANDOM_SEED,
    #                               dtype=np.float64)

    # Trying Mallet
    return gensim.models.wrappers.LdaMallet(MALLET_PATH, corpus=corpus, num_topics=num_topics,
                                            id2word=word_map, random_seed=RANDOM_SEED)


def search_best_lda_model(min_num_topics, topic_limit, word_map, corpus, clean_data):
    models = []
    coherences = []

    for num_topics in range(min_num_topics, topic_limit + 1):
        print("=== Now training model with {:d} topics".format(num_topics))
        start = timer()

        model = lda_topic_modelling(num_topics, word_map, corpus)

        stop = timer()
        print("Elapsed time for {:d} topics: {:.3f} seconds".format(num_topics, (stop - start)))
        models.append(models)

        coherence_model_lda = CoherenceModel(model=model, texts=clean_data, dictionary=word_map)
        print("Coherence Score: {:.5f}".format(coherence_model_lda.get_coherence()))
        coherences.append(coherence_model_lda.get_coherence())

    # Plot
    x_axis = range(min_num_topics, topic_limit + 1)
    plt.plot(x_axis, coherences)
    plt.xlabel("Number of Topics")
    plt.ylabel("Coherence Score")
    plt.legend("Extrinsic Measure", loc='best')
    plt.show()


def visualize_model(model, word_map, corpus):
    print("=== Topics and most important words ===")
    pprint(model.print_topics())
    prepared_data = pyLDAvis.gensim.prepare(model, list(corpus), word_map)
    pyLDAvis.show(prepared_data)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("connection_string_file", help="A text file containing the connection string in one line")
    parser.add_argument("section", type=int, help="Section to analyze")
    args = parser.parse_args()

    dataframe = from_table(args.connection_string_file, NEWS_TABLE)

    section_documents = dataframe[dataframe.section == args.section].contents

    stemmer = SpanishStemmer()

    word_map, corpus, clean_data = prepare_text(section_documents, stemmer)

    min_num_topics = 10
    topic_limit = 20

    search_best_lda_model(min_num_topics, topic_limit, word_map, corpus, clean_data)


if __name__ == '__main__':
    main()
