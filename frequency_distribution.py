import argparse
import logging
import text_preprocessing
from database_reader import from_table
from nltk import FreqDist
from nltk.corpus import stopwords
from nltk import collocations

from functools import partial
from timeit import default_timer as timer
from tqdm import tqdm


def read_documents_from(connection_string_file, table, section, limit=None):
    df = from_table(connection_string_file, table)
    documents = df[df.section == section]

    if limit:
        documents = documents[:limit]

    return documents


def create_logger(logfile):
    logger = logging.getLogger("FreqDist")
    logger.setLevel(logging.INFO)

    f_handler = logging.FileHandler(logfile, mode='w')
    f_handler.setLevel(logging.INFO)

    logger.addHandler(f_handler)

    return logger


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('connection_string_file', help="File containing the information to login to the database")
    parser.add_argument('logfile', help="Log file")

    args = parser.parse_args()

    logger = create_logger(args.logfile)

    documents = read_documents_from(args.connection_string_file, 'news', 1)

    # Preprocessing
    spanish_stopwords = set(stopwords.words('spanish'))
    spanish_stopwords.update(text_preprocessing.SPANISH_NUMBERS)
    no_stopwords = partial(text_preprocessing.remove_stopwords, spanish_stopwords)
    preprocessing_functions = [text_preprocessing.tokenize, text_preprocessing.remove_numbers,
                               text_preprocessing.remove_punctuation, text_preprocessing.to_lower,
                               no_stopwords]

    preprocessed_documents = [text_preprocessing.preprocess_text(d, preprocessing_functions) for d in
                              tqdm(documents.contents.values)]

    all_words = [w for d in preprocessed_documents for w in d]

    start = timer()
    fdist = FreqDist(all_words)
    end = timer()

    real_frequencies = fdist.copy()
    n = fdist.N()

    for key in real_frequencies:
        real_frequencies[key] = fdist[key] / n

    real_frequencies.plot(100, cumulative=False, title='Frequency plot')

    logger.info(fdist.most_common(100))
    logger.info("Elapsed time: {:.3f} seconds".format(end - start))

    bigrams = collocations.BigramAssocMeasures()
    finder = collocations.BigramCollocationFinder.from_words(all_words)
    best100_bigrams = finder.nbest(bigrams.likelihood_ratio, 100)
    logger.info("== Bigrams ==")
    logger.info(best100_bigrams)
    # bf = FreqDist(best100_bigrams)
    #
    # bf.plot(20, cumulative=False, title='Bigram Frequency Plot')
    #
    # print(bf.most_common())

    # print(sorted(finder.ngram_fd.items(), key=lambda t: (-t[1], t[0]))[:20])

    # logger.info(bf.most_common(20))

    total_bigrams = sum(finder.ngram_fd.values())
    logger.info('Total number of bigrams: {:d}'.format(total_bigrams))

    for b in best100_bigrams:
        f = finder.ngram_fd[b]
        # f = f / total_bigrams
        logger.info('({}, {:.3f})'.format(b, f))


if __name__ == '__main__':
    main()
