import argparse
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from timeit import default_timer as timer
from database_reader import from_table
from text_preprocessing import prepare_text, clean_text, tokenize, to_lower, remove_numbers,\
    remove_punctuation, remove_stopwords, preprocess_text, stem, SPANISH_NUMBERS
from nltk.stem.snowball import SpanishStemmer
from nltk.corpus import stopwords
from functools import partial
from tqdm import tqdm

def train_doc2vec_model(documents_df, tags_df, save_file):
    tagged_documents = [TaggedDocument(doc, [tags])
                        for doc, tags in zip(documents_df, tags_df)]

    print(tagged_documents)
    print("=> Now training Doc2Vec Model. Please wait...")
    start = timer()
    d2v_model = Doc2Vec(tagged_documents, vector_size=100, epochs=1000)
    print("Done. Model took {:.3f} seconds to train".format(timer() - start))
    d2v_model.delete_temporary_training_data()
    d2v_model.save(save_file)
    return d2v_model


def load_doc2vec_model(model_file):
    d2vmodel = Doc2Vec.load(model_file)
    return d2vmodel


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--connection_string_file",
                        help="File containing the string to connect to the database")
    parser.add_argument("-s", "--save_file", help="Path where the trained model will be saved")
    parser.add_argument("-t", "--table", help="Section of the DB to be read",
                        default='news')
    parser.add_argument("-p", action="store_true", help="If set, documents will be preprocessed")

    args = parser.parse_args()

    dataframe = from_table(args.connection_string_file, args.table)

    # dataframe.index = dataframe.title.str.len()
    # dataframe = dataframe.sort_index(ascending=False).reset_index(drop=True)
    # print(dataframe[:10][['id', 'title']])
    # section_lengths = dataframe.title.str.len().sort_values(ascending=False)[:10]
    # print(section_lengths)
    # section_documents = sorted(dataframe.title, key=lambda x: len(x), reverse=True)[:20]
    #
    # for d in section_documents:
    #     print("{}: {:d}".format(d, len(d)))

    section_documents_df = dataframe.sort_values('id').contents

    if args.p:
        _, _, section_documents_df = prepare_text(section_documents_df, SpanishStemmer())
    else:
        spanish_stopwords = set(stopwords.words('spanish'))
        spanish_stopwords.update(SPANISH_NUMBERS)
        no_stopwords = partial(remove_stopwords, spanish_stopwords)
        stem_spanish = partial(stem, SpanishStemmer())
        print("Preprocessing documents. Please wait...")
        preprocessing_functions = [tokenize, remove_punctuation, remove_numbers, to_lower, no_stopwords, stem_spanish]
        section_documents_df = [preprocess_text(d, preprocessing_functions) for d in tqdm(section_documents_df)]

    # tags_df = dataframe.sort_values('id')[['id', 'section']]
    tags_df = dataframe.sort_values('id')['id'].values

    # print(section_documents_df)

    train_doc2vec_model(section_documents_df, tags_df, args.save_file)
