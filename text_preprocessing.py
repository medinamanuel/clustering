import string
import gensim
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from gensim import corpora
from functools import partial, reduce
from typing import List


P_TABLE = str.maketrans('', '', string.punctuation)
SPANISH_NUMBERS = ['uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve', 'diez'
                   'once', 'doce', 'trece', 'catorce', 'quince', 'dieciséis', 'dieciseis', 'diecisiete'
                   'dieciocho', 'diecinueve', 'veinte', 'treinta', 'cuarenta', 'cincuenta', 'sesenta'
                    'setenta', 'ochenta', 'noventa', 'cien', 'mil', 'millón', 'millones',
                   'billón', 'billones', 'trillón', 'trillones']


def clean_text(stemmer, stop_words, text):
    # Tokenize
    tokens = word_tokenize(text, 'spanish')

    # Lower case
    tokens = [w.lower() for w in tokens]

    # Get rid of punctuation
    tokens_without_punctuation = [w.translate(P_TABLE) for w in tokens]
    words = [w for w in tokens_without_punctuation if w.isalpha()]

    words = [w for w in words if w not in stop_words]

    words = [stemmer.stem(w) for w in words]

    # TODO: Create bigrams and trigrams to address collocations

    bigram = gensim.models.Phrases(words)
    trigram = gensim.models.Phrases(bigram[words])

    bigram_model = gensim.models.phrases.Phraser(bigram)
    trigram_model = gensim.models.phrases.Phraser(trigram)

    words = bigram_model[words]

    return words


def tokenize(text) -> List[str]:
    return word_tokenize(text, 'spanish')


def to_lower(tokens) -> List[str]:
    return [t.lower() for t in tokens]


def remove_punctuation(tokens) -> List[str]:
    return [w.translate(P_TABLE) for w in tokens]


def remove_numbers(tokens) -> List[str]:
    return [w for w in tokens if w.isalpha()]


def remove_stopwords(stop_words, tokens) -> List[str]:
    return [w for w in tokens if w not in stop_words]


def stem(stemmer, tokens) -> List[str]:
    return [stemmer.stem(t) for t in tokens]


def preprocess_text(text, *preprocessing_functions):
    preprocess = reduce(lambda f, g: lambda x: g(f(x)), *preprocessing_functions)
    return preprocess(text)


def prepare_text(documents, stemmer):
    # Stopwords
    stop_words = set(stopwords.words('spanish'))
    stop_words.update(SPANISH_NUMBERS)

    clean = partial(clean_text, stemmer, stop_words)

    clean_data = [clean(d) for d in documents]

    # LDA inputs: Dictionary and Corpus
    # Dictionary
    word_map = corpora.Dictionary(clean_data)

    word_map.filter_extremes(no_below=100, no_above=.5)

    # Corpus
    corpus = [word_map.doc2bow(d) for d in clean_data]

    return word_map, corpus, clean_data


def main():
    text = "Yo no quiero ir a jugar a la casa porque tengo mucha tarea. A la 1 debo estar estudiando"
    text = '(11/OCT/2011).'
    preprocessing_functions = [tokenize, remove_punctuation, remove_numbers]
    a = preprocess_text(text, preprocessing_functions)
    print(a)


if __name__ == '__main__':
    main()
