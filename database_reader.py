import sqlalchemy
import pandas
import argparse


def read_connection_string(connection_string_file) -> 'string':
    with open(connection_string_file, 'r') as f:
        connection_string = f.read().splitlines()[0]

    return connection_string


def from_table(connection_string_file, table) -> pandas.core.frame.DataFrame:
    connection_string = read_connection_string(connection_string_file)
    e = sqlalchemy.create_engine(connection_string)

    df = pandas.read_sql_table(table, e)

    print("Read {:d} documents from table {}".format(len(df.contents), table))

    df = df.sort_values('id')
    df = df.drop_duplicates(subset='contents', keep='first')

    print(" {:d} documents remained after removing duplicates".format(len(df.contents)))

    return df


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('connection_string_file')

    args = parser.parse_args()

    contents = from_table(args.connection_string_file, 'news')

    print(type(contents))

    assert(len(contents) != 0)
