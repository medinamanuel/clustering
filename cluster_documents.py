import argparse
import numpy as np
import pandas as pd
import logging
import matplotlib.pyplot as plt

from tqdm import tqdm

from gensim import corpora
from gensim.models import CoherenceModel

from doc2vec import load_doc2vec_model
from topic_model import search_best_lda_model, lda_topic_modelling
from sklearn.manifold import TSNE
from mpl_toolkits.mplot3d import axes3d
from timeit import default_timer as timer
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import PCA
from database_reader import from_table
from text_preprocessing import prepare_text, tokenize, remove_punctuation, remove_numbers, preprocess_text, stem,\
    to_lower, remove_stopwords, SPANISH_NUMBERS

from nltk.stem.snowball import SpanishStemmer
from nltk.corpus import stopwords

from pyclustering.cluster.center_initializer import kmeans_plusplus_initializer
from pyclustering.cluster import cluster_visualizer_multidim
from pyclustering.cluster.xmeans import xmeans, splitting_type
from pyclustering.cluster.kmeans import kmeans
from pyclustering.utils import read_sample
from pyclustering.cluster.silhouette import silhouette_ksearch_type, silhouette_ksearch
from functools import partial, reduce

RANDOM_SEED = 2344


def search_best_cluster_model(min_num_clusters, max_num_clusters, d_vectors, logger):
    scores = []

    for n_clusters in range(min_num_clusters, max_num_clusters + 1):
        logger.info("=== Now training model with {:d} clusters === ".format(n_clusters))
        km = KMeans(n_clusters=n_clusters, random_state=RANDOM_SEED)
        start = timer()
        model = km.fit(d_vectors)
        stop = timer()
        labels = model.labels_
        score = silhouette_score(d_vectors, labels, metric='cosine')
        logger.info("Done. Elapsed time: {:.3f} seconds".format(stop - start))
        logger.info("-> Silhouette Score: {:.3f}".format(score))
        scores.append(score)

    # Plot
    x_axis = range(min_num_clusters, max_num_clusters + 1)
    plt.plot(x_axis, scores)
    plt.xlabel("Number of Clusters")
    plt.ylabel("Silhouette Score")
    plt.legend("Silhouette Score", loc='best')
    plt.show()


def kmeans_clustering(d_vectors, n_clusters):
    km = KMeans(n_clusters=n_clusters, random_state=RANDOM_SEED)
    model = km.fit(d_vectors)

    return model


def find_optimal_k(d_vectors, min_clusters, max_clusters):
    search = silhouette_ksearch(d_vectors, min_clusters, max_clusters, algorithm=silhouette_ksearch_type.KMEANS).process()
    print("Scores:")
    print(search.get_scores())


def reduce_dimensionality(d_vectors, n_components, algorithm='pca'):
    if algorithm == 'pca':
        reducer = PCA(n_components=n_components, random_state=RANDOM_SEED)
    else:
        reducer = TSNE(n_components=n_components, random_state=RANDOM_SEED, perplexity=40)

    return reducer.fit_transform(np.array(d_vectors))


def xmeans_clustering(d_vectors, suggested_k):

    # 7 seems good for the first 100 titles
    init_centers = kmeans_plusplus_initializer(d_vectors, suggested_k).initialize()
    xmeans_instance = xmeans(d_vectors, init_centers, ccore=True)

    xmeans_instance.process()

    clusters = xmeans_instance.get_clusters()
    centers = xmeans_instance.get_centers()

    # visualizer = cluster_visualizer_multidim()
    # visualizer.append_clusters(clusters, d_vectors)
    # visualizer.append_cluster(centers, None, marker='*')
    # visualizer.show(max_row_size=3)

    return clusters


def create_tfidf_matrix(documents, *preprocessing_functions):
    stop_words = set(stopwords.words('spanish'))
    stop_words.update(SPANISH_NUMBERS)
    stop_words = list(stop_words)
    preprocess = reduce(lambda f, g: lambda x: g(f(x)), *preprocessing_functions)
    vectorizer = TfidfVectorizer(min_df=0.1, max_df=0.8, stop_words=stop_words, tokenizer=preprocess,
                                 ngram_range=(1, 3))

    # words = [word for document in documents for word in document]
    return vectorizer.fit_transform(documents)


def k_means_with_tfidf(tfidf_matrix, n_clusters, logger):
    km = KMeans(n_clusters=n_clusters, random_state=RANDOM_SEED)
    logger.info("=== Creating {:d} clusters with K~Means ===")
    start = timer()
    km.fit(tfidf_matrix)
    logger.info("Done! Elapsed time: {:.3f} seconds".format(timer() - start))

    return km.labels_, km.cluster_centers_


def print_cluster_documents(cluster, documents, logger):
    for id in cluster:
        document = documents.iloc[id]
        logger.info("-- Document id: {:d}".format(document.id))
        logger.info("-- Title: {}".format(document.title))
        # logger.info("-- Contents:\n{}".format(document.contents))
        logger.info("--------------------------------")


def create_input_for_lda(documents_for_vectors):
    # all_words = [word for document in documents_for_vectors for word in document]
    all_words = documents_for_vectors
    tokens = [tokenize(d) for d in all_words]
    word_map = corpora.Dictionary(tokens)
    corpus = [word_map.doc2bow(t) for t in tokens]

    return word_map, corpus


def create_logger(logfile):
    logger = logging.getLogger("Clustering")
    logger.setLevel(logging.INFO)

    f_handler = logging.FileHandler(logfile, mode='w')
    f_handler.setLevel(logging.INFO)

    logger.addHandler(f_handler)

    return logger


def read_documents_from(connection_string_file, table, section, limit):
    df = from_table(connection_string_file, table)
    documents = df[df.section == section]

    if limit:
        documents = documents[:limit]

    return documents


def print_xmeans_clusters(clusters, documents, logger):
    logger.info("Number of clusters created {:d}".format(len(clusters)))
    logger.info("Clusters: ")
    logger.info(clusters)

    for i, c in enumerate(clusters):
        logger.info("==== Cluster #{:d} ({:d} documents)".format(i, len(c)))
        print_cluster_documents(c, documents, logger)


def cluster_with_doc2vec(d2v_model, documents, preprocessed_documents,
                         clustering_algorithm, n_clusters, reduce_algorithm, n_components, logger):
    d2v_model = load_doc2vec_model(d2v_model)
    d_vectors = []
    logger.info("Inferring vectors from documents")
    start = timer()

    for d in tqdm(preprocessed_documents):
        # vector = d2v_model.infer_vector(d.split())
        vector = d2v_model.infer_vector(d)
        d_vectors.append(vector)

    logger.info("Done! Elapsed time: {:.3f} seconds".format(timer() - start))

    if reduce_algorithm:
        d_vectors = reduce_dimensionality(d_vectors, n_components=n_components, algorithm=reduce_algorithm)

    if clustering_algorithm == 'xmeans':
        clusters = xmeans_clustering(d_vectors, suggested_k=n_clusters)
        print_xmeans_clusters(clusters, documents, logger)
        n_created_clusters = len(clusters)
    else:
        clusters = kmeans_clustering(d_vectors, n_clusters)
        n_created_clusters = n_clusters

    logger.info("Number of clusters created {:d}".format(n_created_clusters))
    logger.info("Clusters: ")
    logger.info(clusters)


def tfidf_matrix_to_list(tfidf_matrix):
    tfidf_matrix_list = []
    for row in range(tfidf_matrix.shape[0]):
        this_row = []
        for col in range(tfidf_matrix.shape[1]):
            this_row.append(tfidf_matrix[row, col])

        tfidf_matrix_list.append(this_row)

    return tfidf_matrix_list


def cluster_with_tfidf(documents, clustering_algorithm, n_clusters, preprocessing_functions, logger):
    tfidf_matrix = create_tfidf_matrix(documents.contents.values, preprocessing_functions)
    print(tfidf_matrix.shape)

    if clustering_algorithm == 'xmeans':
        # X-Means with TF-IDF
        tfidf_list = tfidf_matrix_to_list(tfidf_matrix)
        clusters = xmeans_clustering(tfidf_list, suggested_k=n_clusters)
        n_created_clusters = len(clusters)

        documents['cluster'] = 0

        for i, cluster in enumerate(clusters):
            for row_id in cluster:
                documents.iloc[row_id, lambda df: df.columns.get_loc('cluster')] = i

    else:
        model = kmeans_clustering(tfidf_matrix, n_clusters=10)
        clusters = model.labels_

        n_created_clusters = n_clusters

        documents['cluster'] = clusters

    logger.info("*** Created {:d} clusters with {} ***".format(n_created_clusters, clustering_algorithm))
    logger.info(clusters)

    documents = documents.sort_values('cluster')
    logger.info(documents['cluster'].value_counts())

    for cluster in documents['cluster'].unique():
        this_cluster = documents[documents['cluster'] == cluster].title
        logger.info("=== Cluster {:d} ({:d} documents) ===".format(cluster, len(this_cluster)))
        for d in this_cluster:
            logger.info(d)


def preprocess_documents(documents, preprocessing_functions, logger):
    logger.info("Preprocessing documents. Please wait")
    start = timer()
    documents_for_vectors = [preprocess_text(d, preprocessing_functions) for d in tqdm(documents.title.values)]
    logger.info("Done. Elapsed time: {:.3f} seconds".format(timer() - start))
    id2document = [(i, d) for i, d in zip(documents.id.values, documents_for_vectors)]
    id2document = dict(id2document)

    return documents_for_vectors, id2document


def get_preprocessing_functions():
    stem_text = partial(stem, SpanishStemmer())
    spanish_stopwords = set(stopwords.words('spanish'))
    spanish_stopwords.update(SPANISH_NUMBERS)
    no_stopwords = partial(remove_stopwords, spanish_stopwords)
    return [tokenize, remove_punctuation, remove_numbers, to_lower, no_stopwords, stem_text]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("connection_string_file", help="File containing the string to connect to the database")
    parser.add_argument("-a", "--clustering_algorithm", help="Clustering algorithm. One of kmeans or xmeans", default="xmeans",
                        choices=['kmeans', 'xmeans'])
    parser.add_argument("-n", "--n_clusters", type=int, help="Number of clusters to create")
    parser.add_argument("-dr", "--document_representation", help="d2v for Doc2Vec, tfidf for TF-IDF", default="d2v",
                        choices=['d2v', 'tfidf'])
    parser.add_argument("-d2v", help="Full path to a Doc2Vec trained model")
    parser.add_argument("-table", default='news', help="Table of the DB to be read")
    parser.add_argument("-section", type=int, help="Section to read", default=1)
    parser.add_argument("-nd", "--n_docs", type=int, help="Maximum number of documents to work with", default=10)
    parser.add_argument("-r", "-reduce_algorithm", help="Algorithm to reduce dimensionality",
                        choices=['pca', 'tsne'])
    parser.add_argument("-n_components", type=int, default=20, help="Maximum number of components after reduction")
    parser.add_argument("-l", "--logfile", help="Log file", default="logger_clustering.log")

    args = parser.parse_args()

    logger = create_logger(args.logfile)

    logger.info("Program executed with the following arguments:")
    for arg in vars(args):
        logger.info("{}: {}".format(arg, getattr(args, arg)))

    documents = read_documents_from(args.connection_string_file, args.table, args.section, args.n_docs)

    # Preprocessing
    preprocessing_functions = get_preprocessing_functions()
    documents_for_vectors, id2document = preprocess_documents(documents, preprocessing_functions, logger)

    if args.document_representation == 'd2v':
        cluster_with_doc2vec(args.d2v, documents, documents_for_vectors, args.clustering_algorithm,
                             args.n_clusters, args.reduce_algorithm, args.n_components, logger)


        # print("==== Search best LDA model ====")
        # clean_data = id2document[documents.iloc[i].id]
        # word_map, corpus = create_input_for_lda(clean_data)
        # lda_model = lda_topic_modelling(3, word_map, corpus)
        # coherence_model_lda = CoherenceModel(model=lda_model, texts=clean_data, dictionary=word_map)
        # logger.info("Coherence Score: {:.5f}".format(coherence_model_lda.get_coherence()))
        # logger.info(lda_model.print_topics())
        # # search_best_lda_model(2, 7, word_map=word_map, corpus=corpus, clean_data=clean_data)
    else:
        # TF-IDF
        cluster_with_tfidf(documents, args.clustering_algorithm, args.n_clusters, preprocessing_functions, logger)

        # search_best_cluster_model(5, 50, tfidf_matrix, logger)


    # find_optimal_k(d_vectors, 5, 20)

    # min_num_clusters = 5
    # max_num_clusters = 20
    # search_best_cluster_model(min_num_clusters, max_num_clusters, d_vectors)
    # model = kmeans_clustering(d_vectors, 20)
    # labels = model.labels_
    #
    # score = silhouette_score(d_vectors, labels, metric='cosine')
    #
    # print("Silhouette Score: {:.3f}".format(score))

    # kmeans = pd.DataFrame(labels)
    # # centroids = model.cluster_centers_
    #
    # tsne = TSNE(n_components=3, random_state=RANDOM_SEED, perplexity=40)
    # d_vectors_tsne = tsne.fit_transform(np.array(d_vectors))
    #
    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # ax.scatter(d_vectors_tsne[:, 0], d_vectors_tsne[:, 1], d_vectors_tsne[:, 2], marker='o', c=kmeans[0])
    #
    # plt.show()


if __name__ == '__main__':
    main()
