import argparse
from doc2vec import load_doc2vec_model
from text_preprocessing import preprocess_text, tokenize, remove_numbers, remove_punctuation
from nltk.stem.snowball import SpanishStemmer


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("d2v", help="Full path to a Doc2Vec trained model")

    args = parser.parse_args()

    text = "huracan"
    preprocessing_functions = [tokenize, remove_punctuation, remove_numbers]

    clean_data = preprocess_text(text, preprocessing_functions)

    d2v_model = load_doc2vec_model(args.d2v)

    print(clean_data)

    new_vector = d2v_model.infer_vector(clean_data, epochs=1000)
    # print(new_vector)
    similar = d2v_model.docvecs.most_similar(positive=[new_vector])
    print(similar)
    print(d2v_model.docvecs.doctags)
    # print(d2v_model.docvecs.vectors_docs)
    # print(d2v_model.docvecs.index_to_doctag(9))
    # print(d2v_model.wv.most_similar('secuestr'))


if __name__ == '__main__':
    main()
